import ballerinax/kafka;
import ballerina/io;
import ballerina/random;

public type User record {|
    string first_name = "";
    string last_name = "";
    int age = 0;
|};

public type Shipping record {|
    string city = "";
    string street = "";
    int erf = 0;
|};

public type Item record {
    string sku = "";
    int quantity = 0;
    int store=0;
};

public type Order record {|
    int userId = 0;
    int orderId = 0;
    boolean isValid = false;
    Shipping shipTo = {};
    Item[] items = [];
|};

// Create a subtype of `kafka:AnydataProducerRecord`.
public type UserProducerRecord record {|
    *kafka:AnydataProducerRecord;
    User value;
|};

# Description
#
# + value - Field Description
public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    Order value;
|};

kafka:Producer producer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    check UI();
}

function UI() returns error? {
    io:println("What would you like to do?");
     io:println("1. Register user");
     io:println("2. View catalogue");
     io:println("3. Make order");
     io:println("4. Reprocess order");
    string choice = io:readln("Enter choice 1-4: ");

    // assignments.push({courseCode: courseCode, studentID: loggedID, assignmentID: checkpanic int:fromString(assignmentID), content: content});
    if (choice == "1") {
        check RegisterUI();
    } else if (choice == "2") {
        check Catalogue();
    } else if (choice == "3") {
        check OrderUI();
    } else {
        io:println("Invalid choice");
    }

    check UI();
}

function RegisterUI() returns error? {
    string first_name = io:readln("First name: ");
    string last_name = io:readln("Last name: ");
    string age = io:readln("Age: ");

    UserProducerRecord producerRecord = {
        topic: "registration",
        value: {
            last_name,
            first_name,
            age:  checkpanic int:fromString(age)
        }
    };
    // Sends the message to the Kafka topic.
    check producer->send(producerRecord);
}

function Catalogue()  returns error? {

}

function OrderUI()  returns error? {
    Order orderRecord = {};
    orderRecord.userId = checkpanic int:fromString(io:readln("User ID: "));

    while true {
         string sku = io:readln("SKU: ");
         string quantity = io:readln("Quantity: ");

         orderRecord.items.push({sku: sku, quantity: checkpanic int:fromString(quantity)});

        string choice = io:readln("Enter '1' order another item or '0' to proceed: ");
         if(choice == "0"){
            break;
         }
    }

    orderRecord.shipTo.city = io:readln("City: ");
    orderRecord.shipTo.street = io:readln("Street: ");
    orderRecord.shipTo.erf =  checkpanic int:fromString(io:readln("Erf: "));
    orderRecord.orderId = check random:createIntInRange(100,700);

    OrderProducerRecord producerRecord = {
        topic: "order",
        value: orderRecord
    };

    check producer->send(producerRecord);
}
